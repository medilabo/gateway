package com.openclassrooms.medilabo.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfiguration {

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("patient-service", r -> r.path("**")
                        .filters(f -> f.rewritePath("(?<path>.*)", "/api/patients${path}"))
                        .uri("http://patient:8080"))
                .route("openapi-patient-service", r -> r.path("/v3/api-docs/patients/**")
                        .filters(f -> f.rewritePath("/v3/api-docs/patients(?<path>.*)", "/api/v3/api-docs${path}"))
                        .uri("http://patient:8080"))
                .route("auth-service", r -> r.path("**")
                        .filters(f -> f.rewritePath("(?<path>.*)", "/api/auth/${path}"))
                        .uri("http://auth:8080"))
                .route("openapi-auth-service", r -> r.path("/v3/api-docs/auth/**")
                        .filters(f -> f.rewritePath("/v3/api-docs/auth(?<path>.*)", "/api/v3/api-docs${path}"))
                        .uri("http://auth:8080"))
                .route("note-service", r -> r.path("**")
                        .filters(f -> f.rewritePath("(?<path>.*)", "/api/notes/${path}"))
                        .uri("http://note:8080"))
                .route("openapi-note-service", r -> r.path("/v3/api-docs/notes/**")
                        .filters(f -> f.rewritePath("/v3/api-docs/notes(?<path>.*)", "/api/v3/api-docs${path}"))
                        .uri("http://note:8080"))
                .route("risk-service", r -> r.path("**")
                        .filters(f -> f.rewritePath("(?<path>.*)", "/api/risk/${path}"))
                        .uri("http://risk:8080"))
                .route("openapi-risk-service", r -> r.path("/v3/api-docs/risk/**")
                        .filters(f -> f.rewritePath("/v3/api-docs/risk(?<path>.*)", "/api/v3/api-docs${path}"))
                        .uri("http://risk:8080"))
                .build();
    }
}
